<?php

// TODO: abstract class is a special class that serves as the base class for its derived classes.

abstract class Product
{
    abstract public function check_price($total);
}

class Television extends Product
{
    public function check_price($total)
    {
        return 3000000 * $total;
    }
}

class WashMachine extends Product
{
    public function check_price($total)
    {
        return 1500000 * $total;
    }
}

$product01 = new Television();
echo $product01->check_price(3);

echo '</br>';

$product02 = new WashMachine();
echo $product02->check_price(2);
