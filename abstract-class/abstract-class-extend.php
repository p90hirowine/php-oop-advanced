<?php

abstract class Product
{
    protected static $total_product = 100;
    abstract protected function check_price();

    public static function check_total_product()
    {
        return 'Total product : '.self::$total_product;
    }
}

abstract class Television extends Product
{
    abstract protected function check_type();
}

class TelevisionLED extends Television
{
    public function check_type()
    {
        return 'TV LED';
    }

    public function check_price()
    {
        return 3000000;
    }
}

$product01 = new TelevisionLED();
echo $product01->check_price();
echo '</br>';
echo $product01->check_type();
echo '</br>';
echo Product::check_total_product();
