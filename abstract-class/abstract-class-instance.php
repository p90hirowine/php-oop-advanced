<?php

// TODO: abstract class is a special class that serves as the base class for its derived classes.

abstract class Product
{
}

class Television extends Product
{
}

$product01 = new Television();
