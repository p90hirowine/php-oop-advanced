<?php

// TODO: magic constant is php built-in constants containing various information about the current code


trait HardCover
{
    public function check_trait()
    {
        $msg = 'This massage from '.__METHOD__.' method';
        $msg .= ' inside '.__TRAIT__.' trait';

        return $msg;
    }
}

class Buku
{
    use HardCover;

    public function check_class()
    {
        $msg = 'This massage from '.__METHOD__.' method';
        $msg .= ' inside '.__CLASS__.' class';

        return $msg;
    }
}

$product01 = new Buku();
echo $product01->check_trait();
echo '</br>';
echo $product01->check_class();
