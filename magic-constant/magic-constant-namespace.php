<?php

// TODO: magic constant is php built-in constants containing various information about the current code

namespace Duniailkom;

class Buku
{
    public function check_class()
    {
        $msg = 'This massage from '.__CLASS__.' class';
        $msg .= ' inside '.__NAMESPACE__.' namespace';

        return $msg;
    }
}

$product01 = new Buku();
echo $product01->check_class();
