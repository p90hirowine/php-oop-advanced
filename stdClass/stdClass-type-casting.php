<?php

$foo = 'Learn OOP PHP';
$bar = (object) $foo;

$bar->title = 'Programming';

var_dump($bar);

echo $bar->scalar;

$product = [
    'brand' => 'Oppo',
    'type' => 'Find X',
    'price' => 13499000
];

$product = (object) $product;

var_dump($product);

echo $product->price;
