<?php

declare(strict_types=1);

class SmartPhone
{
    public string $brand;
}

$product01 = new SmartPhone();
$product01->brand = 100;

echo $product01->brand;
