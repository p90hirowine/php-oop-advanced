<?php

declare(strict_types=1);

class SmartPhone
{
    public int|float $total;
}

$product01 = new SmartPhone();

$product01->total = 100;
var_dump($product01->total);

$product01->total = 40.5;
var_dump($product01->total);

$product01->total = '100';
var_dump($product01->total);
