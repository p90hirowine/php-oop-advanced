<?php

function countAverage(array $data)
{
    $tmp = 0;

    for ($i = 0; $i < count($data); $i++) {
        $tmp += $data[$i];
    }

    return $tmp / count($data);
}

$test = countAverage([3,6,12]);

echo $test;
