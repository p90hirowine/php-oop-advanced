<?php

class Product
{
    public $brand;
    public $type;
    public $price;

    public function __construct($brand, $type, $price)
    {
        $this->brand = $brand;
        $this->type = $type;
        $this->price = $price;
    }
}

interface SmartElectronic
{
    public function checkOS();
}

class Television extends Product implements SmartElectronic
{
    public function checkOS()
    {
        return 'Android 9.0 (pie)';
    }
}

function printProduct(SmartElectronic $product)
{
    return 'Product '.$product->brand.' - '.$product->type.' - '.$product->checkOS().' - price : Rp. '.number_format($product->price, 2, ',', '.');
}

$product02 = new Television('Samsung', 'LED TV 14 inch UA40M5000', '4499000');

echo printProduct($product02);
