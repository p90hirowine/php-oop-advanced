<?php

class Company
{
    private $companyName;
    private $companyCity;

    public function __construct($companyName, $companyCity)
    {
        $this->companyName = $companyName;
        $this->companyCity = $companyCity;
    }

    public function companyInfo()
    {
        return $this->companyName.' from '.$this->companyCity.' city';
    }
}

class SmartPhone
{
    private $brand;
    private $supplier;

    public function __construct($brand, Company $supplier)
    {
        $this->brand = $brand;
        $this->supplier = $supplier;
    }

    public function smartPhoneInfo()
    {
        return 'Smartphone '.$this->brand.' supplied by '.$this->supplier->companyInfo();
    }
}

$supplier01 = new Company('CV. Jaya Abadi', 'Bandung');

$sproduct01 = new SmartPhone('Samsung', $supplier01);

echo $sproduct01->smartPhoneInfo();
