<?php

class Product
{
}

interface SmartElectronic
{
    public function checkOS();
}

trait LowWat
{
    public function efficiency()
    {
        return 'Power Consumption : 0.8';
    }
}

class Television extends Product implements SmartElectronic
{
    use LowWat;

    public function checkOS()
    {
        return 'Android 9.0 (pie)';
    }
}

$product01 = new Television();

echo var_dump($product01 instanceof Product);
echo var_dump($product01 instanceof Television);
echo var_dump($product01 instanceof SmartElectronic);
echo var_dump($product01 instanceof LowWat);
echo var_dump($product01 instanceof SmartPhone);
