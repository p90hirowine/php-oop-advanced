<?php

class SmartPhone
{
    public $brand;
    public $type;
    public $price;

    public function __construct($brand, $type, $price)
    {
        $this->brand = $brand;
        $this->type = $type;
        $this->price = $price;
    }
}

class Television
{
    public $brand;
    public $type;
    public $price;

    public function __construct($brand, $type, $price)
    {
        $this->brand = $brand;
        $this->type = $type;
        $this->price = $price;
    }
}

function printSmartPhone(SmartPhone $phone)
{
    return 'Smartphone '.$phone->brand.' - '.$phone->type.' - price : Rp. '.number_format($phone->price, 2, ',', '.');
}

function printTelevision(Television $TV)
{
    return 'Television '.$TV->brand.' - '.$TV->type.' - price : Rp. '.number_format($TV->price, 2, ',', '.');
}

$product01 = new SmartPhone('Samsung', 'M23', '3100000');
$product02 = new Television('Samsung', 'LED TV 40 inch UA40M5000', '4499000');

echo printSmartPhone($product01);
echo '</br>';
echo printTelevision($product02);
