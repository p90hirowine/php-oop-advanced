<?php

class SmartPhone
{
    public $brand;
    public $type;
    public $price;

    public function __construct($brand, $type, $price)
    {
        $this->brand = $brand;
        $this->type = $type;
        $this->price = $price;
    }
}

function printSmartPhone($phone)
{
    return 'Smartphone '.$phone->brand.' - '.$phone->type.' - price : Rp. '.number_format($phone->price, 2, ',', '.');
}

$product01 = new SmartPhone('Samsung', 'M23', '3100000');
$product02 = new SmartPhone('Xiaomi', 'Redmi Note 8', '2300000');
$product03 = new SmartPhone('Apple', 'iPhone X', '15700000');

echo printSmartPhone($product01);
echo '</br>';
echo printSmartPhone($product02);
echo '</br>';
echo printSmartPhone($product03);
