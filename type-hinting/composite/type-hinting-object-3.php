<?php

abstract class product
{
    public $brand;
    public $type;
    public $price;

    public function __construct($brand, $type, $price)
    {
        $this->brand = $brand;
        $this->type = $type;
        $this->price = $price;
    }

    abstract public function getInfoProduct();

    public function getInfo()
    {
        return $this->brand.' - '.$this->type.' - price : Rp. '.number_format($this->price, 2, ',', '.');
    }
}

class SmartPhone extends Product
{
    public function getInfoProduct()
    {
        return 'Smartphone '.$this->getInfo();
    }
}

class Television extends Product
{
    public function getInfoProduct()
    {
        return 'Television '.$this->getInfo();
    }
}

class PrintProductInfo
{
    public $listProduct = [];

    public function addProduct(Product $product)
    {
        $this->listProduct[] = $product;
    }

    public function print()
    {
        $str = 'List Product : '.'</br>';

        foreach ($this->listProduct as $val) {
            $str .= '- '.$val->getInfoProduct().'</br>';
        }

        return $str;
    }
}

$product01 = new SmartPhone('Samsung', 'M23', '3100000');
$product02 = new Television('Samsung', 'LED TV 14 inch UA40M5000', '4499000');

$printProduct = new PrintProductInfo();
$printProduct->addProduct($product01);
$printProduct->addProduct($product02);

echo $printProduct->print();
