<?php

function sum(int $a, int $b)
{
    return $a + $b;
}

echo sum(12, 10);
echo '</br>';
echo sum(6.5, 1, 3);
echo '</br>';
echo sum('5', 6);
