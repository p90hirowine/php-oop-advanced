<?php

class Laptop
{
    public function __construct(
        private $brand
    ) {
        echo 'Product'.$this->brand.' has been created'.'</br>';
    }
}

$product01 = new Laptop('Asus');
$product02 = new Laptop('Asus');

if ($product01 === $product02) {
    echo 'both objects are the same';
} else {
    echo 'the two objects are not the same';
}
