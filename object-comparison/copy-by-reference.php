<?php

// TODO: copy by reference is the process of filling the memory address of a variable to another variable. This memory address location is also known as a reference

$a = 5;
$b =& $a;

$a = 10;

echo $a;
echo '</br>';
echo $b;
