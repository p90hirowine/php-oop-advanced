<?php

class Product
{
    private $brand;

    public function __construct($brand)
    {
        $this->brand = $brand;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand($brand)
    {
        $this->brand = $brand;
    }
}

$product01 = new Product('Asus');
$product02 = $product01;

if ($product01 === $product02) {
    echo 'both objects are the same';
} else {
    echo 'the two objects are not the same';
}

echo '</br>';
echo $product01->getBrand();
echo '</br>';
echo $product02->getBrand();
echo '</br>';

$product02->setBrand('Acer');

echo $product01->getBrand();
echo '</br>';
echo $product02->getBrand();
