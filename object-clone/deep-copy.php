<?php

class Company
{
    public $name;

    public function __construct($name)
    {
        $this->name = $name;
    }
}

class Laptop
{
    public $brand;
    public $supplier;

    public function __construct($brand)
    {
        $this->brand = $brand;
    }

    public function __clone()
    {
        $this->supplier = clone $this->supplier;
    }
}

$supplierLaptop = new Company('CV. Jaya Abadi');
$product01 = new Laptop('Acer');
$product01->supplier = $supplierLaptop;

$product02 = clone $product01;
$product02->brand = 'Asus';
$product02->supplier->name = 'CV. Maju Makmur';

var_dump($product01);
var_dump($product02);
