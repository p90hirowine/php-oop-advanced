<?php

// TODO: magic method clone() is a method that will be executed when an object is cloned with the clone command.

class Laptop
{
    private $brand;

    public function __construct($brand)
    {
        $this->brand = $brand;
    }

    public function __clone()
    {
        echo 'Cloned '.__CLASS__.' object'.'</br>';
    }
}

$product01 = new Laptop('Acer');
$product02 = clone $product01;
$product03 = clone $product02;
