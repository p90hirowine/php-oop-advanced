<?php

class Company
{
    public $name;

    public function __construct($name)
    {
        $this->name = $name;
    }
}

class Laptop
{
    public $brand;
    public $supplier;

    public function __construct($brand)
    {
        $this->brand = $brand;
    }
}

$supplierLaptop = new Company('CV. Jaya Abadi');
$product01 = new Laptop('Acer');
$product01->supplier = $supplierLaptop;

var_dump($product01);
