<?php

class Product
{
    protected static $brand = 'LG';

    public static function checkBrand()
    {
        return 'Product with '.self::$brand.' brand is available';
    }

    public static function getInfo()
    {
        return static::checkBrand();
    }
}

class WashMachine extends Product
{
    public static function checkBrand()
    {
        return 'Wash Machine with '.self::$brand.' brand is available';
    }
}

echo Product::getInfo();
echo '</br>';
echo WashMachine::getInfo();
