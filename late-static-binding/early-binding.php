<?php

class Product
{
    protected $brand = 'LG';

    public function checkBrand()
    {
        return 'Product with '.$this->brand.' brand is available';
    }
}

class WashMachine extends Product
{
    public function checkBrand()
    {
        return 'Wash Machine with '.$this->brand.' brand is available';
    }
}

$product01 = new Product();
echo $product01->checkBrand();

echo '</br>';

$product02 = new WashMachine();
echo $product02->checkBrand();
