<?php

class Product
{
    protected $brand = 'LG';

    public function checkBrand()
    {
        return 'Product with '.$this->brand.' brand is available';
    }

    public function getInfo()
    {
        return $this->checkBrand();
    }
}

class WashMachine extends Product
{
    public function checkBrand()
    {
        return 'Wash Machine with '.$this->brand.' brand is available';
    }
}

$product01 = new Product();
echo $product01->getInfo();

echo '</br>';

$product02 = new WashMachine();
echo $product02->getInfo();
