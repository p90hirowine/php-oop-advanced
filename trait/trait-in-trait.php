<?php

// TODO: a trait within a trait

trait SmartElectronic
{
    public function check_os()
    {
        return 'Android 9.0 (pie)';
    }
}

trait LowWatt
{
    use SmartElectronic;

    public function efficiency()
    {
        return $this->check_os().' - power consumption 0.8';
    }
}

class SmartTV
{
    use LowWatt;
}

$product01 = new SmartTV();
echo $product01->efficiency();
