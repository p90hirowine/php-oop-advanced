<?php

// TODO: a trait can have abstract methods

trait SmartElectronic
{
    public function check_os()
    {
        return 'Android 9.0 (pie)';
    }

    abstract public function check_processor();
}

class SmartTV
{
    use SmartElectronic;

    public function check_processor()
    {
        return 'Snapdragon 845';
    }
}

$product01 = new SmartTV();
echo $product01->check_os();
echo '</br>';
echo $product01->check_processor();
