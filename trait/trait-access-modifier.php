<?php

// TODO: access modifier can be changed when using the trait

trait SmartElectronic
{
    public function check_os()
    {
        return 'Android 9.0 (pie)';
    }
}

class SmartTV
{
    use SmartElectronic {check_os as protected;}
}

$product01 = new SmartTV();
echo $product01->check_os();
