<?php

// TODO: if there is a method with the same name between the parent class, trait, and the class itself, the order of priority starts from the method in the class itself (1), the method belongs to the trait (2), the method belongs to the parent class (3).


class Television
{
    public function efficiency()
    {
        return 'power consumption 0.8';
    }
}

trait SmartElectronic
{
    public function check_os()
    {
        return 'Android 9.0 (pie)';
    }
}

trait LowWatt
{
    public function efficiency()
    {
        return 'power consumption 0.9';
    }
}

class SmartTV extends Television
{
    use SmartElectronic;
    use LowWatt;

    public function check_info()
    {
        return 'Smart TV '.$this->check_os().' - '.$this->efficiency();
    }

    public function efficiency()
    {
        return 'power consumption 0.7';
    }
}

$product01 = new SmartTV();
echo$product01->efficiency();
