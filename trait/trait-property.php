<?php

// TODO: a trait can have properties

trait SmartElectronic
{
    public $internet = 'Telkom Indihome';

    public function check_os()
    {
        return 'Android 9.0 (pie)';
    }
}

class SmartTV
{
    use SmartElectronic;
}

$product01 = new SmartTv();
echo $product01->internet;
echo '</br>';
echo $product01->check_os();
