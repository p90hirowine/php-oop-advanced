<?php

// TODO: a solution if in different traits there are methods with the same name


trait SmartElectronic
{
    public function check_os()
    {
        return 'Android 9.0 (pie)';
    }

    public function efficiency()
    {
        return 'power consumption 0.8';
    }
}

trait LowWatt
{
    public function efficiency()
    {
        return 'power consumption 0.9';
    }
}


class SmartTV
{
    use SmartElectronic, LowWatt {
        SmartElectronic::efficiency insteadof LowWatt;
        LowWatt::efficiency as efficiencyLow;
    }
}

$product01 = new SmartTV();
echo $product01->efficiency();
echo '</br>';
echo $product01->efficiencyLow();
