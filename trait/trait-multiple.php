<?php

// TODO: trait is a php solution to handle multiple inheritance constraint

class Television
{
    public function check_resolution()
    {
        return 'Full HD';
    }
}

trait SmartElectronic
{
    public function check_os()
    {
        return 'Android 9.0 (pie)';
    }
}

trait LowWatt
{
    public function efficiency()
    {
        return 'power consumption 0.9';
    }
}

class SmartTV extends Television
{
    use SmartElectronic;
    use LowWatt;

    public function check_info()
    {
        return 'Smart TV '.$this->check_resolution().' - '.$this->check_os().' - '.$this->efficiency();
    }
}

$product01 = new SmartTV();
echo $product01->check_info();
