<?php

// TODO: trait is a php solution to handle multiple inheritance constraint

class Television
{
    public function check_resolution()
    {
        return 'Full HD';
    }
}

trait SmartElectronic
{
    public function check_os()
    {
        return 'Android 9.0 (pie)';
    }
}

class SmartTV extends Television
{
    use SmartElectronic;

    public function check_info()
    {
        return 'Smart TV '.$this->check_resolution().' - '.$this->check_os();
    }
}

$product01 = new SmartTV();
echo $product01->check_info();
