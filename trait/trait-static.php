<?php

// TODO: a trait can have static properties and static methods

trait SmartElectronic
{
    public static $internet = 'Telkom Indihome';

    public static function check_os()
    {
        return 'Android 9.0 (pie)';
    }
}

echo SmartElectronic::$internet;
echo '</br>';
echo SmartElectronic::check_os();
