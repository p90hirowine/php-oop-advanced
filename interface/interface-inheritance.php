<?php

// TODO: interfaces can also be derived into other interfaces

interface exportProduct
{
    public function check_price_usd();
    public function check_country();
}

interface foodProduct
{
    public function check_expired();
}

interface frozenFoodProduct extends foodProduct
{
    public function check_min_temp();
}

class Nugget implements exportProduct, frozenFoodProduct
{
    public function check_price_usd()
    {
        return 7.5;
    }

    public function check_country()
    {
        return ['Singapore', 'Malaysia', 'Thailand'];
    }

    public function check_expired()
    {
        return 'April 2023';
    }

    public function check_min_temp()
    {
        return -14;
    }
}

$product01 = new Nugget();

echo $product01->check_expired();
echo '</br>';
echo $product01->check_min_temp();
