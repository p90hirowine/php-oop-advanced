<?php

class Product
{
}

interface LaptopGaming
{
}

class Laptop extends Product implements LaptopGaming
{
}

$laptop01 = new Laptop();

var_dump(is_subclass_of($laptop01, 'Product'));
var_dump(is_subclass_of($laptop01, 'LaptopGaming'));
var_dump(is_subclass_of($laptop01, 'Laptop'));
var_dump(is_subclass_of($laptop01, 'Television'));
