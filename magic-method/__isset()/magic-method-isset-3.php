<?php

// TODO: magic method isset() is a special method that will be executed when a property does not exist or cannot be accessed from a class is checked with isset() or empty() function


class Product
{
    public $brand = 'Samsung';
    protected $stok = 10;
    private $type = 'Mouse';

    public function __isset($name)
    {
        echo 'Does the '.$name.' property exist ?';
        return isset($this->$name);
    }
}

$product01 = new Product();

var_dump(isset($product01->brand));
var_dump(isset($product01->stok));
var_dump(isset($product01->type));
var_dump(isset($product01->color));
