<?php

// TODO: magic method isset() is a special method that will be executed when a property does not exist or cannot be accessed from a class is checked with isset() or empty() function

class Product
{
    public $brand = 'Sony';
    protected $stocks = 10;
    private $type = 'Playstation';

    public function __isset($name)
    {
        echo 'Does '.$name.' property empty ?';
        return var_dump(empty($this->$name));
    }
}

$product01 = new Product();

var_dump(empty($product01->brand));
empty($product01->stocks);
empty($product01->type);
empty($product01->color);
