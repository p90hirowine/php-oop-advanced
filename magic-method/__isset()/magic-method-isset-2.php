<?php

// TODO: magic method isset() is a special method that will be executed when a property does not exist or cannot be accessed from a class is checked with isset() or empty() function

class Product
{
    protected $brand = 'Samsung';
    protected $stok;
    protected $type = '';
}

$product01 = new Product();

var_dump(isset($product01->brand));
var_dump(isset($product01->stok));
var_dump(isset($product01->type));

var_dump(empty($product01->brand));
var_dump(empty($product01->stok));
var_dump(empty($product01->type));
