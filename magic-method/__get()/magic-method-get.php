<?php

// TODO: the get() method is a magic method that is executed when we access a property that doesn't exist or can't be accessed from outside the class

class product
{
    public $brand = 'Logitech';
    private $price = 300000;
    public function __get($name)
    {
        return 'Sorry, property called '.$name.' doesn\'t exist';
    }
}

$product01 = new Product();

echo $product01->brand;
echo '</br>';
echo $product01->price;
echo '</br>';
echo $product01->type;
