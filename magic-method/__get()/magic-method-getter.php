<?php

// TODO: the get() method is a magic method that is executed when we access a property that doesn't exist or can't be accessed from outside the class

class Product
{
    private $brand = 'Logitech';
    private $price = 150000;
    private $type = 'Mouse';

    public function __get($name)
    {
        if ($name == 'brand') {
            $result = strtoupper($this->brand);
        } elseif ($name == 'price') {
            $result = 'Rp. '.number_format($this->price, 2, ',', '.');
        } elseif ($name == 'type') {
            $result = 'Product type : '.$this->type;
        } else {
            $result = 'Sorry, '.$name. ' property doesn\'t exist';
        }

        return $result;
    }
}

$product01 = new Product();

echo $product01->brand;
echo '</br>';
echo $product01->price;
echo '</br>';
echo $product01->type;
echo '</br>';
echo $product01->feature;
