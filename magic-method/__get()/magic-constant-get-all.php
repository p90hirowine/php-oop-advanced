<?php

// TODO: the get() method is a magic method that is executed when we access a property that doesn't exist or can't be accessed from outside the class

class Product
{
    private $brand = 'Logitech';
    private $type = 'Mouse';

    public function __get($name)
    {
        if ($name == 'brand' || $name == 'type') {
            $result = strtoupper($this->$name);
        } else {
            $result = 'Sorry '.$name. ' property doesn\'t exist';
        }

        return $result;
    }
}

$product01 = new Product();

echo $product01->brand;
echo '</br>';
echo $product01->type;
echo '</br>';
echo $product01->price;
