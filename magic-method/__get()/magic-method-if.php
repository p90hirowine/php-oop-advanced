<?php

// TODO: the get() method is a magic method that is executed when we access a property that doesn't exist or can't be accessed from outside the class

class Product
{
    public function __get($name)
    {
        if ($name == 'brand') {
            $result = 'Logitech';
        } elseif ($name == 'price') {
            $result = 150000;
        } elseif ($name == 'type') {
            $result = 'Mouse';
        } else {
            $result = 'Sorry, '.$name.' property doesn\'t exist';
        }

        return $result;
    }
}

$product01 = new Product();

echo $product01->brand;
echo '</br>';
echo $product01->price;
echo '</br>';
echo $product01->IDproduct;
echo '</br>';
echo $product01->type;
