<?php

// TODO: magic method callStatic() is a special method that is executed when we access a static method that is not available or cannot be accessed from a class

class Product
{
    public function __call($name, $arguments)
    {
        echo 'Sorry '.$name.' method with arguments '.implode(', ', $arguments).' is not available !';
    }

    public static function __callStatic($name, $arguments)
    {
        echo 'Sorry '.$name.' static method with arguments '.implode(', ', $arguments).' is not available !';
    }
}

$product01 = new Product();

$product01->sum(1, 2, 3, 4, 5);
echo '</br>';
Product::sub(1, 2, 3, 4, 5);
