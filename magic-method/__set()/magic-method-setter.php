<?php

// TODO: the set() method will be executed when we try to input a value into a property that doesn't exist or can't be accessed from a class


class Product
{
    private $brand;
    private $price;

    public function __set($name, $value)
    {
        if ($name == 'brand') {
            if (is_string($value)) {
                $this->brand = $value;
            } else {
                echo 'Error : Brand must be string !';
            }
        } elseif ($name == 'price') {
            if (is_int($value)) {
                $this->price = $value;
            } else {
                'Error : Price must be integer !';
            }
        } else {
            echo 'Sorry, '.$name.' property doesn\'t exist !';
        }
    }
}

$product01 = new Product();

$product01->brand = 'Logitech';
$product01->price = 150000;
$product01->type = 'Mouse';

var_dump($product01);
