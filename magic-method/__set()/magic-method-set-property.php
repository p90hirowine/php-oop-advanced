<?php

// TODO: the set() method will be executed when we try to input a value into a property that doesn't exist or can't be accessed from a class

class Product
{
    public $brand;

    public function __set($name, $value)
    {
        $this->$name = strtoupper($value);
    }
}

$product01 = new Product();
$product01->brand = 'Logitech';
$product01->price = 150000;
$product01->type = 'Mouse';

echo $product01->brand;
echo '</br>';
echo $product01->price;
echo '</br>';
echo $product01->type;
