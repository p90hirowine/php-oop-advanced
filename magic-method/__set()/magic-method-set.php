<?php

// TODO: the set() method will be executed when we try to input a value into a property that doesn't exist or can't be accessed from a class

class Sample
{
}

$product01 = new Sample();
$product01->brand = 'Samsung';


class Product
{
    public function __set($name, $value)
    {
        echo 'Sorry, '.$name.' property cannot be filled by '.$value.' value'.'</br>';
    }
}

$product02 = new Product();
$product02->brand = 'Samsung';
$product02->price = 150000;
