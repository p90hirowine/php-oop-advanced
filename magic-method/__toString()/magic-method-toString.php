<?php

// TODO: the toString() method will be executed when an object is required to return a string value.

class Product
{
    public function __toString()
    {
        return 'This from product class';
    }
}

class Television extends Product
{
}

$product01 = new Television();
echo $product01;
