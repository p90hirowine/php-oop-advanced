<?php

// TODO: magic method call() is a special method that is executed when we access a method that does not exist or cannot be accessed from a class

class Product
{
    public function __call($name, $arguments)
    {
        echo 'Sorry, '.$name.' method doesn\'t exist !';
    }

    public function sum($numbers)
    {
        $result = 0;

        for ($i = 0; $i < count($numbers); $i++) {
            $result += $numbers[$i];
        }

        return $result;
    }
}

$product01 = new Product();

echo $product01->sum([1,2,3,4,5,30,10]);
echo '</br>';
echo $product01->sub();
