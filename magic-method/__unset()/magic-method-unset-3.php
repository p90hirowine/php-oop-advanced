<?php

// TODO: magic method unset() is a method that will be executed when a property that does not exist or cannot be accessed from the class is deleted with the unset() function

class Product
{
    private $brand = 'Sony';

    public function __unset($name)
    {
        echo 'Sorry '.$name.' property doesn\'t exist / cannot access !';
    }
}

$product01 = new Product();

echo "<pre>";
print_r($product01);
echo "</pre>";

unset($product01->stok);

echo "<pre>";
print_r($product01);
echo "</pre>";
