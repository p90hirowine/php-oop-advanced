<?php

// TODO: method chaining is a term to refer to writing methods that can be connected or linked to each other.

class DisplayDatabase
{
    private $query;

    public function select($field)
    {
        $this->query = "SELECT $field ";
        return $this;
    }

    public function from($table)
    {
        $this->query .= "FROM $table ";
        return $this;
    }

    public function where($condition)
    {
        $this->query .= "WHERE $condition";
        return $this;
    }

    public function getQuery()
    {
        return $this->query;
    }
}

$student01 = new DisplayDatabase();

$student01->select('nim, name')->from('students')->where('nim = 0046096940');

echo $student01->getQuery();
