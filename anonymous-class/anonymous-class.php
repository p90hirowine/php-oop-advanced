<?php

class Product
{
    protected $brand;

    public function __construct($brand)
    {
        $this->brand = $brand;
    }
}

interface HaveBrand
{
    public function getBrand();
}


$product01 = new class ('Hitachi') extends Product implements HaveBrand {
    public function getBrand()
    {
        return $this->brand;
    }
};

echo $product01->getBrand();
echo '</br>';

function printSmartPhone($handPhone)
{
    return 'Smartphone '.$handPhone->brand.' '.$handPhone->type.' Price : Rp. '.number_format($handPhone->price, 2, ',', ',');
}

echo printSmartPhone(new class () {
    public $brand = 'Samsung';
    public $type = 'M23';
    public $price = '3500000';
});
