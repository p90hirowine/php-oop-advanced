<?php

class Company
{
    public function __construct(
        public $companyName,
        public $companyCity
    ) {
    }

    public function getCompanyInfo()
    {
        return $this->companyName.' from '.$this->companyCity.' city';
    }
}

class Product
{
    public ?Company $supplier;

    public function __construct(Company $supplier)
    {
        $this->supplier = $supplier;
    }
}

$supplier01 = new Company('CV Jaya Abadi', 'Bandung');

$product01 = new Product($supplier01);

echo $product01->supplier->getCompanyInfo();
