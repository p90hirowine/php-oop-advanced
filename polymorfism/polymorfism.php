<?php

// TODO: Simply put, polymorphism is a collection of methods that have the same name, but their implementation varies depending on the object of the method

abstract class Product
{
    abstract protected function check_price();
    abstract protected function check_brand();
    abstract protected function check_stocks();
}

class Television extends Product
{
    public function check_price()
    {
        return 3000000;
    }

    public function check_brand()
    {
        return 'Samsung';
    }

    public function check_stocks()
    {
        return 100;
    }
}

class WashMachine extends Product
{
    public function check_price()
    {
        return 1500000;
    }

    public function check_brand()
    {
        return 'LG';
    }

    public function check_stocks()
    {
        return 50;
    }
}


class refrigerator extends Product
{
    public function check_price()
    {
        return 4000000;
    }

    public function check_brand()
    {
        return 'Sharp';
    }

    public function check_stocks()
    {
        return 30;
    }
}

$product01 = new Television();
$product02 = new WashMachine();
$product03 = new refrigerator();

function show_brand($object_product)
{
    return $object_product->check_brand().'</br>';
}

echo show_brand($product01);
echo show_brand($product02);
echo show_brand($product03);
